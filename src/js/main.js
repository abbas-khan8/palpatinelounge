window.onload = function(e) 
{   
    // Tabs
    var tabList = document.getElementsByClassName("tablinks");
    for(let i = 0; i < tabList.length; i++)
    {
        console.log(tabList[i].innerHTML);
    }

    // Reactions
    var reactions = document.getElementsByClassName("emoji");
    var reactionsCounter = document.getElementsByClassName("emoji-counter");

    for(let i = 0; i < reactions.length; i++)
    {
        reactions[i].addEventListener("mouseover", function(e) 
        {
            e.target.parentElement.style.border = "0.5px solid #ffffff";
        });

        reactions[i].addEventListener("mouseleave", function (e) 
        {
            e.target.parentElement.style.border = "none";
        });

        reactions[i].addEventListener("click", function(e) 
        {                
            increaseReactionCount(i);
        });
    }

    function increaseReactionCount(id)
    {
        let current = reactionsCounter[id].innerHTML;

        current = parseInt(current);

        current ++;
        
        reactionsCounter[id].innerHTML = current
    }

    // To-do list
    let todoItems = [];    

    function renderTodo(todo) 
    {
        const list = document.querySelector('.todo-list-items');      
        const item = document.querySelector(`[data-key='${todo.id}']`);

        if (todo.deleted) 
        {
            item.remove();
            if (todoItems.length === 0)
            {
                clear.style.width = "0%";               
                clear.style.visibility = "hidden";

                list.innerHTML = '';
            } 
            return;
        }

        if (todoItems.length > 0)
        {      
            clear.style.width = "150px";      
            clear.style.visibility = "visible";
        } 

        const isChecked = todo.checked ? 'done': '';

        const node = document.createElement("li");
        node.setAttribute('class', `todo-item ${isChecked}`);
        node.setAttribute('data-key', todo.id);
        
        node.innerHTML = `
            <input id="${todo.id}" type="checkbox"/>
            <label for="${todo.id}" class="tick tick-todo"></label>
            <span>${todo.text}</span>
            <button class="delete-todo">
            <svg><use href="#delete-icon"></use></svg>
            </button>
        `;

        if (item) 
        {
            list.replaceChild(node, item);
        } 
        else 
        {
            list.append(node);
        }       
    }

    function addTodo(text) 
    {
        const todo = 
        {
            text,
            checked: false,
            id: Date.now(),
        };
        
        todoItems.push(todo);
        renderTodo(todo);
    }

    function deleteTodo(key) {
        const index = todoItems.findIndex(item => item.id === Number(key));

        const todo = {
            deleted: true,
            ...todoItems[index]
        };

        todoItems = todoItems.filter(item => item.id !== Number(key));
        renderTodo(todo);
    }
      
    function toggleDone(key) 
    {
        const index = todoItems.findIndex(item => item.id === Number(key));
        todoItems[index].checked = !todoItems[index].checked;

        renderTodo(todoItems[index]);
    }

    const form = document.querySelector('.todo-form');    

    form.addEventListener('submit', event => 
    {        
        event.preventDefault();

        const input = document.querySelector('.todo-form-input');
        const text = input.value.trim();

        let valid = true;

        if (text == '') 
        {
            alert("Todo item can not be empty!");
            valid = false;
        }
        else if (text.length >= 200) 
        {
            alert("Todo item exceeds character count!");
            valid = false;
        }
        if (valid == true) 
        {
            addTodo(text);
            input.value = '';
            input.focus();
        }
    });

    const list = document.querySelector('.todo-list-items');

    list.addEventListener('click', event => {
        if (event.target.classList.contains('tick-todo')) 
        {
            const itemKey = event.target.parentElement.dataset.key;
            toggleDone(itemKey);
        }

        if (event.target.classList.contains('delete-todo')) 
        {
            const itemKey = event.target.parentElement.dataset.key;
            deleteTodo(itemKey);
        }
    });

    const clear = document.getElementById('clear'); 

    clear.addEventListener('click', event =>
    { 
        todoItems.forEach(element => deleteTodo(element.id));
    });

    //Dummy data
    let dummyItems = [ 
        "Locate Khyber crystal",
        "Ask mum to help tailor clothing fit for a Sith Lord",
        "Become Supreme Chancellor for the Senate",
        "Execute Order 66"
    ];

    // Populate list with dummy data
    for(let i = 0; i < dummyItems.length; i++)
    {
        let text = dummyItems[i];
        let dummyId = Math.floor(Math.random() * 256);

        const todo = 
        {
            text,
            checked: false,
            id: dummyId,
        };

        todoItems.push(todo);
        renderTodo(todo);
    }
}
